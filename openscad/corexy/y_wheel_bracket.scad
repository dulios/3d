/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include <variables.scad>;

//head mount
mount_x=50;

module plate(w_d=wheel_diameter, w_h=wheel_height, w_r=wheel_rim, p=profile_side, w=wall_width, pulley_r=pulley_bore_rim, mount_x=mount_x, m3=m3, m5=m5, pulley_d=pulley_diameter){
  difference(){
    //plate base
    plate_size=mount_x+abs(mount_x-(p+2*w+2*w_d));
    union(){
      minkowski(){
	m_d=w;
	m_h=w/4;
	union(){
	  m_x=m_d;
	  m_y=m_x;
	  m_z=m_h;
	  //base
	  cube([plate_size+4*w-m_x, plate_size-m_y, w-m_z], center=true);
	  translate([0, +plate_size/2, (p+3*w/2)/2-w/2])
	    cube([plate_size+p+4*w-m_x, 1.1*w-m_y, p+3*w/2-m_z], center=true);
	}
	cylinder(d=m_d, h=m_h, center=true);
      }
      //wheel mount
      wheel_post_z=(w+p-w_h)/2;
      wheel_x=(w_d+p+4*w)/2;
      wheel_y=(w_d+p)/2-1;
      wheel_z=(wheel_post_z+w)/2;
      translate([wheel_x, wheel_y, wheel_z])
	cylinder(d2=w_r, d1=w_d, h=wheel_post_z, center=true );
      translate([-wheel_x, wheel_y, wheel_z])
	cylinder(d2=w_r, d1=w_d, h=wheel_post_z, center=true );
      translate([wheel_x, -wheel_y, wheel_z])
	cylinder(d2=w_r, d1=w_d, h=wheel_post_z, center=true );
      translate([-wheel_x, -wheel_y, wheel_z])
	cylinder(d2=w_r, d1=w_d, h=wheel_post_z, center=true );
  }
  //holes
  #union(){
    //wheel mount bolts
    bolt_height=m5x45;
    bolt_x=(w_d+p+4*w)/2;
    bolt_y=(w_d+p)/2-1;
    bolt_z=0;
    translate([bolt_x, bolt_y, bolt_z])
      cylinder(d=m5, h=bolt_height, center=true );
    translate([-bolt_x, bolt_y, bolt_z])
      cylinder(d=m5, h=bolt_height, center=true );
    translate([bolt_x, -bolt_y, bolt_z])
      cylinder(d=m5, h=bolt_height, center=true );
    translate([-bolt_x, -bolt_y, bolt_z])
      cylinder(d=m5, h=bolt_height, center=true );
    //belt bolts
    belt_bolt_x=(plate_size+p+2*w-m3)/2;
    rotate([-90,0,0]){
      translate([belt_bolt_x, -(w+p/2)+(pulley_diameter-m3)/2, plate_size/2])
	cylinder(d=m3, h=w+1, center=true);
      translate([belt_bolt_x, -(w+p/2)-(pulley_diameter-m3)/2, plate_size/2])
	cylinder(d=m3, h=w+1, center=true);
      translate([-belt_bolt_x, -(w+p/2)+(pulley_diameter-m3)/2, plate_size/2])
	cylinder(d=m3, h=w+1, center=true);
      translate([-belt_bolt_x, -(w+p/2)-(pulley_diameter-m3)/2, plate_size/2])
	cylinder(d=m3, h=w+1, center=true);
      // central bolt
      translate([0, -(w+p/2), plate_size/2])
	cylinder(d=m3, h=w+1, center=true);
    }
    //head mount bolts
    bolt_size=m4;
    x=10; //mounting bolts separation
    y=10;
    x_max=bolt_x-(w_d+bolt_size+w)/2;
    y_max=bolt_y;
    for(pos_x=[-(x_max-(x_max%x)): x: x_max]){
      for(pos_y=[-(y_max-(y_max%y)): y: y_max]){
    	translate([pos_x, pos_y, 0])
    	  cylinder(d=m4, h=1+w, center=true );
      }
    }
  }
  }
}

module rail(p=profile_side, w=wall_width, w_d=wheel_diameter, w_h=wheel_height){
  //x rail
  translate([0, 0, p/2+w])
    cube([10*p, p, p], center=true);

  wheel_x=(w_d+p+4*w)/2;
  wheel_y=(w_d+p)/2-1;
  wheel_z=(p+2*w)/2;
  translate([wheel_x, wheel_y, wheel_z])
    cylinder(d=w_d, h=w_h, center=true );
  translate([-wheel_x, wheel_y, wheel_z])
    cylinder(d=w_d, h=w_h, center=true );
  translate([wheel_x, -wheel_y, wheel_z])
    cylinder(d=w_d, h=w_h, center=true );
  translate([-wheel_x, -wheel_y, wheel_z])
    cylinder(d=w_d, h=w_h, center=true );
}
//preview
plate();
if($preview){
#rail();
}
