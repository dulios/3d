/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include <variables.scad>;

module v6_heatsink(top_d=v6_mount_top_diameter, top_h=v6_mount_top_height, middle_d=v6_mount_middle_diameter, middle_h=v6_mount_middle_height, bottom_d=v6_mount_bottom_diameter, bottom_h=v6_mount_bottom_height, heatsink_d=v6_mount_heatsink_diameter, heatsink_h=v6_mount_heatsink_height){
     cylinder(d=middle_d, h=middle_h, center=true);
     translate([0, 0, (middle_h+top_h)/2])
       cylinder(d=top_d, h=top_h, center=true);
     translate([0, 0, -(middle_h+bottom_h)/2])
       cylinder(d=bottom_d, h=bottom_h, center=true);
     translate([0, 0, -(middle_h+heatsink_h)/2-bottom_h])
       cylinder(d=heatsink_d, h=heatsink_h, center=true);
}

module fan(fan=fan_side, fan_bolt=fan_bolt_spacing, fan_w=fan_width, fan_hole=fan_hole, bolt=m3){
  difference(){
    cube([fan, fan, fan_w], center=true);
    union(){
      translate([fan_bolt/2, fan_bolt/2, 0])
	cylinder(d=bolt, h=1+fan_w, center=true);
      translate([fan_bolt/2, -fan_bolt/2, 0])
	cylinder(d=bolt, h=1+fan_w, center=true);
      translate([-fan_bolt/2, fan_bolt/2, 0])
	cylinder(d=bolt, h=1+fan_w, center=true);
      translate([-fan_bolt/2, -fan_bolt/2, 0])
	cylinder(d=bolt, h=1+fan_w, center=true);
      cylinder(d=fan_hole, h=1+fan_w, center=true);
    }
  }
}

module v6_mount(w=wall_width, bolt=m4, bolt_head=m4_head, middle_d=v6_mount_middle_diameter, middle_h=v6_mount_middle_height, bolt_spacing=carrier_bolt_spacing, heatsink_d=v6_mount_heatsink_diameter, heatsink_h=v6_mount_heatsink_height, screw=m4, ){
     mount_holder=(heatsink_d+w)/2;
  difference(){
    union(){
      translate([0, (heatsink_d+w)/4, 0])
	cube([bolt_spacing+bolt_head+w, mount_holder, middle_h], center=true);
    }
    //holes
    union(){
      //v6_mount
      cylinder(d=middle_d, h=1+middle_h, center=true);
      //screws
      translate([bolt_spacing/2,mount_holder/2,0])
	   rotate([90,0,0])
	   cylinder(d=screw, h=1+mount_holder, center=true);
      translate([-bolt_spacing/2,mount_holder/2,0])
	   rotate([90,0,0])
	   cylinder(d=screw, h=1+mount_holder, center=true);
    }
  }
}

//preview
//v6_mount();
v6_mount();
//#v6_heatsink();
//translate([0, (v6_mount_heatsink_diameter+fan_width)/2, -fan_side/2+4.3])
//rotate([90, 0, 0])
//#fan();
