/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

rod_diameter=8;
profile_side=20;
wall_width=5;
m4_head=11.5;
separation=10; //rod center should be 10mm from extrusion

$fn=60;

module rod(d=rod_diameter, h=profile_side){
  cylinder(d=d, h=h*1.5, center=true);
}

module m4x10(d=4, h=10, head_d=m4_head, head_h=10){
  union() {
    translate([0,0,-h/2])
      cylinder(d=d,h=h,center=true);
    translate([0,0,head_h/2])
      cylinder(d=head_d,h=head_h,center=true);
  }
}

module rod_holder(d=rod_diameter, h=profile_side, w=wall_width, screw=m4_head, s=separation){
  difference(){
    union(){
      //plate rods up to 12mm
      translate([0, -s+w/2,0])
	cube([d+w+2*(screw+w),w,h],center=true);
      //rod holder
      cylinder(d=d+2*w, h=h, center=true);
      translate([0,-(d/4+w/2), 0])
	cube([d+2*w, (d+2*w)/2, h], center=true);
      //rod holder screw
      translate([0,d/2+w,0])
	rotate([0,0,0])
	cube([w+w/2, 2*screw, h], center=true);
    }
    #union(){
      //rod
      rod();
      //relief
      translate([0, (1+d/2+w+screw)/2, 0])
	cube([w/4, 1+d/2+w+screw, 1.5*h],center=true);
      //screws
      translate([screw/2+d/2+w,-(s-w),0])
	rotate([-90,0,0])
	m4x10(head_h=20);
      translate([-(screw/2+d/2+w),-(s-w),0])
	rotate([-90,0,0])
	m4x10(head_h=20);
      translate([-(w/2+w/4),d/2+w+screw/2,0])
	rotate([0,-90,0])
	m4x10(head_h=20);
      translate([(w/2+w/4),d/2+w+screw/2,0])
	rotate([0,90,0])
	m4x10(head_h=20);
    }
  }
}

//preview
rod_holder();
//#m3x10_screw();
//#rod();
