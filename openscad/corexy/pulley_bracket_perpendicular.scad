/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include <variables.scad>;

//add washer size
pulley_washer=0;

module pulley_bracket_perpendicular(p=profile_side, w=wall_width,  bolt=m4, pulley_d=pulley_diameter, pulley_rim=pulley_bore_rim, pulley_bore=m4, pulley_h=pulley_height, pulley_washer=pulley_washer, bolt_head=m4_head){
  difference() {
    union(){
      hull(){
	pulley_spacing_x=2+(pulley_d+bolt_head)/2;
	pulley_spacing_y=0;
	translate([pulley_spacing_x-(p-bolt_head)/2, -pulley_spacing_y, 0])
	  cylinder(d=p, h=w, center=true);
	translate([-(pulley_spacing_x-(p-bolt_head)/2), -pulley_spacing_y, 0])
	  cylinder(d=p, h=w, center=true);
      }
      //pulley rim spacing
      spacing=(w-pulley_washer)/2;
      translate([0, 0, (spacing+w)/2])
	cylinder(d2=1.2*pulley_rim, d1=1.2*pulley_rim+spacing, h=spacing, center=true);
    }
    #union(){
      pulley_spacing_x=(pulley_d+bolt_head)/2;
      spacing=pulley_rim-pulley_bore;
      cylinder(d=bolt, h=3*w, center=true);
      translate([pulley_spacing_x, 0, 0])
	cylinder(d=bolt, h=3*w, center=true);
      translate([-pulley_spacing_x, 0, 0])
	cylinder(d=bolt, h=3*w, center=true);
    }
  }
}


//preview
pulley_bracket_perpendicular();
