/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include <variables.scad>;

bearing_rim=624_rim;
bearing_bore=624_bore;
bearing_diameter=624_diameter;

custom_pulley_height=2*624_height;

custom_pulley_diameter=2*nozzle_diameter+(bearing_diameter);

module pulley(bolt=m4, pulley_d=custom_pulley_diameter, pulley_rim=bearing_rim, pulley_bore=bearing_diameter, pulley_h=custom_pulley_height, belt_w=belt_w, belt_h=belt_h){
  difference() {
    union(){
      //body
      cylinder(d=pulley_d, h=pulley_h, center=true);
      //flanges
      flange_w=(pulley_h-belt_w)/2;
      flange_h=1.2*belt_h+pulley_d;
      flange_z=(pulley_h-flange_w)/2;
      translate([0, 0, flange_z])
	cylinder(d1=pulley_d, d2=flange_h, h=flange_w, center=true);
      translate([0, 0, -flange_z])
	cylinder(d1=flange_h, d2=pulley_d, h=flange_w, center=true);
    }
    #union(){
      cylinder(d=pulley_bore, h=1+pulley_h, center=true);
    }
  }
}

//preview
pulley();
