/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include <variables.scad>;

module pulley_bracket(p=profile_side, w=wall_width,  bolt=m4, pulley_d=pulley_diameter, pulley_rim=pulley_bore_rim, pulley_bore=m4, pulley_h=pulley_height){
  difference() {
    union(){
      hull(){
	//pulley mount
	cylinder(d=pulley_d, h=w, center=true);
	//profile mount
	pulley_spacing=(pulley_d+p+w)/2;
	translate([p/2, -pulley_spacing, 0])
	  cylinder(d=p, h=w, center=true);
	translate([-p/2, -pulley_spacing, 0])
	  cylinder(d=p, h=w, center=true);
      }
      //pulley rim spacing
      spacing=pulley_rim-pulley_bore;
      translate([0, 0, (spacing+w)/2])
	cylinder(d2=pulley_rim, d1=pulley_rim+spacing,  h=spacing, center=true);
      //pulley profile spacing
      profile_spacing=pulley_rim-pulley_bore+0.5;
      hull() {
	//profile mount
	pulley_spacing=(pulley_d+p+w)/2;
	translate([p/2, -pulley_spacing, (w+profile_spacing)/2])
	  cylinder(d=p, h=profile_spacing, center=true);
	translate([-p/2, -pulley_spacing, (w+profile_spacing)/2])
	  cylinder(d=p, h=profile_spacing, center=true);
      }
    }
    #union(){
      pulley_spacing=(pulley_d+p+w)/2;
      spacing=pulley_rim-pulley_bore;
      cylinder(d=bolt, h=3*w, center=true);
      translate([p/2, -pulley_spacing, 0])
	cylinder(d=bolt, h=3*w, center=true);
      translate([-p/2, -pulley_spacing, 0])
	cylinder(d=bolt, h=3*w, center=true);
    }
  }
}

//preview
pulley_bracket();
