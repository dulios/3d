/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include <variables.scad>;

module plate_simple(w_d=wheel_diameter, w_h=wheel_height, w_r=wheel_rim, w_i=wheel_insertion, p=profile_side, w=wall_width, w_b=wheel_bore){
  wheel_x_separation=p+4*w+w_d;
  wheel_y_separation=p+w_d-2*w_i;
  difference(){
    union(){
      //plate base
      hull(){
	wheel_base=w_d+2*w;
	
	translate([wheel_x_separation/2, wheel_y_separation/2, 0])
	  cylinder(d=wheel_base, h=w, center=true);
	translate([wheel_x_separation/2, -wheel_y_separation/2, 0])
	  cylinder(d=wheel_base, h=w, center=true);
	translate([-wheel_x_separation/2, wheel_y_separation/2, 0])
	  cylinder(d=wheel_base, h=w, center=true);
	translate([-wheel_x_separation/2, -wheel_y_separation/2, 0])
	  cylinder(d=wheel_base, h=w, center=true);
      }
      //wheel mount
      wheel_post_z=(w+p-w_h)/2;
      wheel_x=wheel_x_separation/2;
      wheel_y=wheel_y_separation/2;
      wheel_z=(wheel_post_z+w)/2;
      translate([wheel_x, wheel_y, wheel_z])
	cylinder(d2=w_r, d1=w_d, h=wheel_post_z, center=true );
      translate([-wheel_x, wheel_y, wheel_z])
	cylinder(d2=w_r, d1=w_d, h=wheel_post_z, center=true );
      translate([wheel_x, -wheel_y, wheel_z])
	cylinder(d2=w_r, d1=w_d, h=wheel_post_z, center=true );
      translate([-wheel_x, -wheel_y, wheel_z])
	cylinder(d2=w_r, d1=w_d, h=wheel_post_z, center=true );
    }
    union(){
      //wheel mount bolts
      bolt_height=m5x45;
      bolt_x=wheel_x_separation/2;
      bolt_y=wheel_y_separation/2;
      bolt_z=0;
      translate([bolt_x, bolt_y, bolt_z])
	cylinder(d=m5, h=bolt_height, center=true );
      translate([-bolt_x, bolt_y, bolt_z])
	cylinder(d=m5, h=bolt_height, center=true );
      translate([bolt_x, -bolt_y, bolt_z])
	cylinder(d=m5, h=bolt_height, center=true );
      translate([-bolt_x, -bolt_y, bolt_z])
	cylinder(d=m5, h=bolt_height, center=true );
    }
  }
}

module y_profile_mount(w=wall_width, p=profile_side, bolt=m4){
  difference(){
    union(){
      //y profile rest
      rest_width=p;
      rest_length=2*p+2*w;
      rest_x=(p+w)/2;
      rest_y=p+3*w/2;
      rest_z=(rest_length-w)/2;
      translate([0, -(rest_y+(p+w)/2), rest_z])
	cube([rest_width+2*w, w, rest_length], center=true);
      translate([rest_x, -rest_y, rest_z])
	cube([w, rest_width, rest_length], center=true);
      translate([-rest_x, -rest_y, rest_z])
	cube([w, rest_width, rest_length], center=true);
    }
    #union(){
      //holes
      bolt_y=p+3*w/2;
      bolt_z=p/2+w;
      translate([0, -bolt_y, bolt_z]){
	rotate([0, 90, 0])
	  cylinder(d=bolt, h=1+p+2*w, center=true);
	rotate([90, 90, 0])
	  cylinder(d=bolt, h=1+p+2*w, center=true);
      }
      translate([0, -bolt_y, bolt_z+p]){
	rotate([0, 90, 0])
	  cylinder(d=bolt, h=1+p+2*w, center=true);
	rotate([90, 90, 0])
	  cylinder(d=bolt, h=1+p+2*w, center=true);
      }
    }
  }
}

module rail(p=profile_side, w=wall_width, w_d=wheel_diameter, w_h=wheel_height){
  //x rail
  translate([0, 0, p/2+w])
    cube([10*p, p, p], center=true);
  //y rail
  y_z=3*p;
  y_y=p;
  translate([0, -(y_y+p+3*w)/2, (y_z-w)/2])
    cube([p, y_y, y_z+1], center=true);

  wheel_x=(w_d+p+4*w)/2;
  wheel_y=(w_d+p)/2-1;
  wheel_z=(p+2*w)/2;
  translate([wheel_x, wheel_y, wheel_z])
    cylinder(d=w_d, h=w_h, center=true );
  translate([-wheel_x, wheel_y, wheel_z])
    cylinder(d=w_d, h=w_h, center=true );
  translate([wheel_x, -wheel_y, wheel_z])
    cylinder(d=w_d, h=w_h, center=true );
  translate([-wheel_x, -wheel_y, wheel_z])
    cylinder(d=w_d, h=w_h, center=true );
}

//preview
plate_simple();
y_profile_mount();
if($preview){
#rail();
}
