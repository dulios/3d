/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include <variables.scad>;

separation_bearing=wall_width+20; //rod center should be 20mm from extrusion
separation_screw=wall_width+10; //linear screw should be 10mm from extrusion
separation_rod=100; //distance from screw to smooth rods

bearing_diameter=lm8uu_diameter; //measure 15
bearing_height=lm8uu_height; //measure 23.7

module rod(d=rod_diameter, h=profile_side){
  cylinder(d=d, h=h*1.5, center=true);
}

module m4x10(d=4, h=10, head_d=m4_washer, head_h=10){
  union() {
    translate([0,0,-h/2])
      cylinder(d=d,h=h,center=true);
    translate([0,0,head_h/2])
      cylinder(d=head_d,h=head_h,center=true);
  }
}

module linear_bearing(b_d=bearing_diameter, b_h=bearing_height){
     cylinder(d=b_d, h=b_h, center=true);
}

module linear_bearing_holder(b_d=bearing_diameter, b_h=bearing_height, w=wall_width, s=separation_bearing, screw=m4_washer){
  difference(){
    hull(){
      //support
      translate([0,w/2-s,0])
	cube([b_d+2*w, w, 2*b_h], center=true);
      //bearing_holder
      cylinder(d=b_d+2*w, h=2*b_h, center=true);
      //tabs
      translate([0, b_d/2+w, 2*b_h/4])
	rotate([0,90,0])
	cylinder(d=screw, h=2*w, center=true);
      translate([0, b_d/2+w, -2*b_h/4])
	rotate([0,90,0])
	cylinder(d=screw, h=2*w, center=true);
    }
    union(){
      //double bearings
      cylinder(d=b_d, h=1+2*b_h, center=true);
      //relief
      translate([0, (1+b_d/2+w+screw/2)/2, 0])
	cube([w/4, 1+b_d/2+w+screw/2, 1+2*b_h], center=true);
      //tab holes
      translate([0, b_d/2+w, 2*b_h/4])
	cube([1+b_d+2*w, 2, 3], center=true);
      translate([0, b_d/2+w, 0])
	cube([1+b_d+2*w, 2, 3], center=true);
      translate([0, b_d/2+w, -2*b_h/4])
	cube([1+b_d+2*w, 2, 3], center=true);
    }
  }
}

module z_screw(n_d=lead_screw_nut_diameter, n_d_l=lead_screw_nut_height, n_s=lead_screw_nut_bolt_spacing, w=wall_width, s=separation_screw, p=profile_side, bolt=m3){
  difference(){
    //base
    hull(){
      //support
      translate([0,w/2-s,0])
	cube([n_d_l+2*w, w, p+w], center=true);
      //bearing_holder
      cylinder(d=n_d_l, h=p+w, center=true);
    }
    //screw mount
    rotate([0,0,45])
    union(){
      cylinder(d=1+n_d, h=1+p+w, center=true);
      translate([n_s/2, 0, 0])
	cylinder(d=bolt, h=1+p+w, center=true);
      translate([-n_s/2, 0, 0])
	cylinder(d=bolt, h=1+p+w, center=true);
      translate([0, n_s/2, 0])
	cylinder(d=bolt, h=1+p+w, center=true);
      translate([0, -n_s/2, 0])
	cylinder(d=bolt, h=1+p+w, center=true);
    }
  }
}

module base_2020(b_d=bearing_diameter, b_h=bearing_height, p=profile_side, w=wall_width, s_r=separation_rod, screw=m4_washer){
  //base
  translate([0, w/2, (p+w)/2])
    cube([2*s_r+b_d+2*w+2*screw, w, p+w], center=true);
  rotate([-90, 0, 0])
    translate([0, -w/2, -p/2])
    cube([2*s_r+b_d+2*w+2*screw, w, p], center=true);
}

module z_bed_holder() {
  difference(){
    union(){
      base_2020();
      translate([separation_rod, separation_bearing, bearing_height])
	linear_bearing_holder();
      translate([-separation_rod, separation_bearing, bearing_height])
	linear_bearing_holder();
      translate([0, separation_screw, (profile_side+wall_width)/2])
	z_screw();
    }
    union(){
      rotate([90,0,0])
	{
	  //screw mount
	  translate([lead_screw_nut_height+wall_width, wall_width+profile_side/2, 0])
	    cylinder(d=4, h=12, center=true);
	  translate([-(lead_screw_nut_height+wall_width), wall_width+profile_side/2, 0])
	    cylinder(d=4, h=12, center=true);
	  //bearing mount
	  translate([separation_rod+bearing_diameter/2+wall_width+m4_washer/2, wall_width+profile_side/2, 0])
	    cylinder(d=4, h=12, center=true);
	  translate([separation_rod-bearing_diameter/2-wall_width-m4_washer/2, wall_width+profile_side/2, 0])
	    cylinder(d=4, h=12, center=true);
	  translate([-(separation_rod+bearing_diameter/2+wall_width+m4_washer/2), wall_width+profile_side/2, 0])
	    cylinder(d=4, h=12, center=true);
	  translate([-(separation_rod-bearing_diameter/2-wall_width-m4_washer/2), wall_width+profile_side/2, 0])
	    cylinder(d=4, h=12, center=true);
	}
      //screw mount
      translate([0, -profile_side/2, 0])
	cylinder(d=4, h=12, center=true);
      //bearing mount
      translate([separation_rod, -profile_side/2, 0])
	cylinder(d=4, h=12, center=true);
      translate([-separation_rod, -profile_side/2, 0])
	cylinder(d=4, h=12, center=true);
    }
  }
}

//preview
//#z_screw();
//#rod(h=24);
//#linear_bearing();
//#base_2020();
//#linear_bearing_holder();

//test
// translate([0, -10, 0])
// z_screw();
// translate([separation_rod, 0, 0])
// linear_bearing_holder();
// translate([-separation_rod, 0, 0])
// linear_bearing_holder();

//final
z_bed_holder();
