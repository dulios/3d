/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//nozzle size to calculate walls in some modules
nozzle_diameter=0.6;

// z linear rods
rod_diameter=8;

// extrusion default T-slot 2020
profile_side=20;

// Used for separation between objects and default object thickness.
wall_width=5;

//bolts sizes
m3=3;
m3_washer=6;
m3_head=7;
m4=4;
m4_washer=11.5;
m4_head=9;
m5=5;
m5_washer=20;
m5_head=10;
m5x45=45;
m6=6;
m6_washer=11.5;
m6_head=10;
m8=8;

//wheels
wheel_diameter=24.4;
wheel_height=11;
wheel_bore=5;
wheel_rim=wheel_bore+2.8;
wheel_insertion=1; //how much does the wheel sinks in the slot

//pulleys
pulley_bore=4;
pulley_bore_rim=5;
pulley_diameter=18;
pulley_height=8.5;

//head mount
mount_x=50;

//belt
belt_w=6;
belt_h=2;

//bearings
//624
624_diameter=13;
624_height=5;
624_bore=4;
624_rim=6;

lm8uu_diameter=15;
lm8uu_height=24; //23.7

//v6 heatsink
v6_mount_top_diameter=16;
v6_mount_top_height=3.5;
v6_mount_middle_diameter=12;
v6_mount_middle_height=6;
v6_mount_bottom_diameter=v6_mount_top_diameter;
v6_mount_bottom_height=7;
v6_mount_heatsink_diameter=22.2;
v6_mount_heatsink_height=25.7;

//carrier
carrier_bolt_spacing=20;

//40mm fan
fan_bolt_spacing=32;
fan_side=40;
fan_width=10.5;
fan_hole=38;

// z lead_screw nut
lead_screw_nut_diameter=10; //10
lead_screw_nut_height=22; //22
lead_screw_nut_bolt_spacing=16; //16 used for bolt spacing

$fn=60;
