/*  Copyright 2023 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

cuerdas=360/6;
$fn=cuerdas;
mink=2;
mink_fn=32;
tolerancia=0.2;
alto=30;
ancho=150;
proporcion=sqrt(2);
tab_x=60;
tab_y=2.84;
tab_z=15;

module tab(x=tab_x+2*tolerancia, y=tab_y+2*tolerancia, z=alto){
  translate([0,0,-z/2])
    cube([x, y, z], center=true);
}

module display(x=100,y=tab_y,z=100){
  translate([0,0,z/2])
    cube([x,y,z], center=true);
}

module base(x=ancho, y=ancho/3, z=alto){
  translate([0,0,-z/2])
  resize([0,y,0])
    linear_extrude(height=z, slices=cuerdas, scale=1/proporcion, center=true)
    circle(d=x, $fn=8);
}

module pala(x=2/3*ancho, y=ancho/6, z=3/5*alto, mink=mink){
  brazo_x=tab_x/8;
  brazo_y=y;
  brazo_z=z/3;

  translate([tab_x/3,-brazo_y/2,brazo_z/2])
    cube([brazo_x,brazo_y,brazo_z], center=true);
  translate([-tab_x/3,-brazo_y/2,brazo_z/2])
    cube([brazo_x,brazo_y,brazo_z], center=true);

  pala_x=x;
  pala_y=y;
  pala_z=z;
  radio=x*proporcion;
  difference(){
    translate([0,pala_y/2,pala_z/2])
      cube([pala_x,pala_y,pala_z], center=true);
    translate([0,radio+radio/100,pala_z])
      resize([0,0,2*radio/proporcion])
	sphere(r=radio, $fn=360/2);
  }
}

module llanta(x=alto, mink=mink){
  radio=x;
  ancho=radio/proporcion;
  ancho_aro=ancho*0.8;

  difference(){
    union(){
      translate([0,0,ancho/2])
	cylinder(r=radio-4*mink, h=ancho, center=true);
      
      translate([0,0,-ancho/2])
	cylinder(r=radio/4-4*mink, h=ancho, center=true);
      
      ancho_2=ancho/8;
      radio_2=radio*1.05;
      translate([0,0,ancho/3])
	cylinder(r=radio_2-4*mink, h=ancho_2, center=true);
      
      translate([0,0,2*ancho/3])
	cylinder(r=radio_2-4*mink, h=ancho_2, center=true);
    }
    translate([0,0,ancho+ancho_aro/4])
      cylinder(r=radio*0.8-4*mink, h=ancho_aro, center=true);
  }
}

module celda(mink=mink, mink_fn=mink_fn){
  union(){
      cylinder(r=7,h=50,center=true);
      translate([0,0,25+1/2])
	cylinder(r=2,h=2,center=true);
    }
  sphere(r=mink, $fn=mink_fn);
}

module sensor(mink=mink, mink_fn=mink_fn){
  difference(){
    cylinder(r=7.5-mink,h=15, center=true);
    translate([0,0,15-3-mink])
      cylinder(r=6-mink,h=15, center=true);
    }
}

module leds(x=tab_x,led_d=7,leds=7,z=alto-tab_z){
  for(i=[-(x/2-led_d/2):(x-led_d)/leds:x/2-led_d/2]){
    translate([i,0,z/2])
    cylinder(d=led_d, h=z, center=true);
    }
  /*translate([0,0,z/4])
    cube([tab_x-led_d,led_d/2,(z)/2], center=true);*/
  translate([0,-10,12.5])
    cube([40,8,25], center=true);
  translate([0,-5,0])
    rotate([90,0,0])
      cylinder(r=5, h=10, center=true);
}

//%display();
module body(){
  translate([0,0,alto]){
    union(){
      difference(){
	    base();
	    tab();
      }
      translate([0,ancho/5,-alto])
        pala();
    }
  }

  translate([ancho-4/5*ancho/proporcion,0,0])
  rotate([0,90,0])
  minkowski(){
    llanta();
    sphere(r=mink, $fn=mink_fn);
  }
  translate([-ancho+4/5*ancho/proporcion,0,0])
  rotate([0,-90,0])
  minkowski(){
    llanta();
    sphere(r=mink, $fn=mink_fn);
  }

  translate([0,-ancho/6+7,alto/2-7])
  rotate([0,90,0])
    celda();
  translate([0,-ancho/6+7,alto/2+7])
  rotate([0,-90,0])
    celda();
}
//end body

difference(){
  difference(){
    body();
    translate([0,0,-75])
      cube([500,500,150], center=true);
  }

  translate([0,20,alto-7.5]){
    rotate([-90,0,0]){
      translate([10,0,0])
	sensor();
      translate([-10,0,0])
	sensor();
    }
  }
  leds();
}

translate([0,15,alto-7.5]){
  rotate([-90,0,0]){
    translate([10,0,0])
    sensor();
    translate([-10,0,0])
    sensor();
  }
 }
