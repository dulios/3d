/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

rod_diameter=8;
end_screw_diameter=5.5;
end_screw_side=10;
end_screw_depth=4;
screw_diameter=3;
wall_width=2;
filament_diameter=1.75+0.5;

$fn=16;

module base(r_d=rod_diameter,e_r=end_screw_diameter/2,w=wall_width,s_d=screw_diameter) {
    piece_side=r_d+2*w;
    r=rod_diameter/2;

    // Rod
    //cylinder(h=piece_side, r=piece_side/2, center=true);
    // Filament Guide Support
    translate([r+e_r+w,0,0])
    rotate([90])
        cylinder(h=piece_side, r=piece_side/2, center=true);
    // Zip Tie Support
    translate([-(r_d+s_d)/2,0,0])
    rotate([90])
        cylinder(h=piece_side, r=piece_side/2, center=true);
}

module remove(r_d=rod_diameter,e_d=end_screw_diameter,e_s=end_screw_side,e_p=end_screw_depth,w=wall_width,s_d=screw_diameter,f_d=filament_diameter, reduction=0.9) {
    piece_side=r_d+2*w;
    r=r_d/2;
    e_r=e_d/2;
    s_r=s_d/2;
    f_r=f_d/2;

    // Rod
    cylinder(h=e_s*3, r=r, center=true);
    // Rod Slot
    translate([-(3/2*r_d),0,0])
        cube([3*r_d, reduction*r_d, 3*(r_d+w)], center=true);
    // Filament
    translate([r+e_r+w,0,0])
    rotate([90])
        cylinder(h=10*(r_d+w), r=f_r, center=true);
    // Zip Tie
    translate([-(r+s_d/2),0,0])
    rotate([90])
        cylinder(h=3*(r_d+w), r=s_r, center=true);
    // PTFE tube adapter fitting
    translate([r+e_r+w, e_p+(piece_side)/2-e_p, 0])
    rotate([90])
        cylinder(h=2*e_p, r=e_r, center=true);
    // Filament funnel
    translate([r+e_r+w, -(piece_side)/4, 0])
    rotate([90])
        cylinder(h=(piece_side)/2+1, r2=r, r1=f_r, center=true);
}

module final() {
  minkowski() {
    difference() {
      hull()
        base();
      minkowski() {
        remove();
        sphere(r=1);
      }
    }
    sphere(r=1);
  }
}

module preview() {
  difference() {
    hull()
      base();
    remove();
  }
}

//base();
//hull() base();
//#remove();

preview();

//final();
