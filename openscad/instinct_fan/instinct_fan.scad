/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

tolerance=0.2;
fn=36;
wall=2;

/*base*/
//external size
e_x=96+2*tolerance;
e_y=34+tolerance;
e_z=50;
//pcb+backplate
pcb_x=98.5+2*tolerance;
pcb_y=4.5+tolerance;

module base(x=e_x,y=e_y,z=wall,p_x=pcb_x,p_y=pcb_y){
    //duct base
    module b(x=x,y=y,z=z){
        cube([x,y,z]);
        }
    //pcb
    module pcb(x=p_x,y=p_y,z=z){
        cube([x,y,z]);
        }
    union(){
        b();
        translate([-abs(p_x-x)/2,-p_y,0])
            pcb();
        }        
    }

/* holes */
//internal size
spacing=2;
spacing_right=8;
i_x0=43;
i_y0=26;
i_x1=53;
i_y1=12;
i_x2=40;
i_y2=17;
module hole(x=wall,y=wall,z=wall+2*tolerance,t=tolerance){
    // add 2*t to z to prevent zfighting
    cube([x,y,z]);
    }

module holes(z=wall+2*tolerance,t=tolerance,s=spacing,s_r=spacing_right){
    //remove s from x0 to add spacing on the left
    //remove s from y0 to add spacing on the top
    translate([s, e_y-i_y0, -t])
        hole(x=i_x0-s,y=i_y0-s,z=z);
    //remove s from y1 to add spacing on the top
    *translate([i_x0,e_y-i_y1,-t])
        hole(x=i_x1-s,y=i_y1-s,z=z);
    translate([e_x-i_x2-s_r,0,-t])
        hole(x=i_x2,y=i_y2,z=z);
    }

//screw positions
screw_d=3+2*tolerance;
screw_l=20;
screw_head_d=5.5+2*tolerance;
screw_pole_d=8;
screw0_x=7.5;
screw0_y=28.5;
screw1_x=24;
screw1_y=screw0_y;
screw2_x=40;
screw2_y=screw0_y;
screw3_x=e_x-3;
screw3_y=26;
module screw_hole(x=0,y=0,z=-screw_l/2,h=screw_l,d=screw_d){
    translate([x,y,z])
            cylinder(d=d,h=h,$fn=fn);
    }
module screw_pole(x=0,y=0,z=-screw_l/2,h=screw_l,d=screw_d,h_d=screw_pole_d){
    translate([x,y,z])
        cylinder(d=h_d,h=h,$fn=fn);

    }

//screw holes
module screws(t=tolerance){
    screw_hole(x=screw0_x,y=e_y-screw0_y,z=-t);
    screw_hole(x=screw1_x,y=e_y-screw1_y,z=-t);
    screw_hole(x=screw2_x,y=e_y-screw2_y,z=-t);
    screw_hole(x=screw3_x,y=e_y-screw3_y,z=-t);
    }

module base_walls(d=wall,z=3*wall,t=tolerance,w=wall){
    module w(){
        minkowski(){
            base(z=z-t);
            cylinder(d=d,h=t,$fn=fn);
            }
        }
    translate([0,0,-z+w]){
        difference(){
            w();
            base(z=z+2*t);
            }
        }
    }

//fan diameter
fan_d=38;
fan_size=40;
fan_screw=32;
fan_screw_d=3+2*tolerance;
fan_screw_head_d=7+2*tolerance;
fan_screw_head_h=15;
duct_length=16;
// adjust for holes in duct
adjust=3.5;
    
module fan_base(x=fan_size,y=fan_size,z=wall,w=wall,t=tolerance){
    translate([w,w])
        minkowski(){
            cube([x-2*w,y-2*w,z-t]);
            cylinder(r=w,h=t,$fn=fn);
            }
    }
module fan_screw_holes(x=fan_size,y=fan_size,d=abs(fan_size-fan_screw)/2,h=duct_length,t=tolerance){
    screw_hole(x=d,y=d,z=-t,h=h+2*t);
    screw_hole(x=x-d,y=d,z=-t,h=h+2*t);
    screw_hole(x=x-d,y=x-d,z=-t,h=h+2*t);
    screw_hole(x=d,y=x-d,z=-t,h=h+2*t);
    }
module fan_screw_posts(x=fan_size,y=fan_size,d=abs(fan_size-fan_screw)/2,h=duct_length,t=tolerance){
    screw_pole(x=d,y=d,z=0,h=h);
    screw_pole(x=x-d,y=d,z=0,h=h);
    screw_pole(x=x-d,y=x-d,z=0,h=h);
    screw_pole(x=d,y=x-d,z=0,h=h);
    }
module fan_screw_posts_remove(x=fan_size,y=fan_size,d=abs(fan_size-fan_screw)/2,z=fan_screw_head_h,h=duct_length,h_d=screw_head_d,w=wall,t=tolerance){
    screw_pole(x=d,y=d,z=z-h,h=h,h_d=h_d);
    screw_pole(x=x-d,y=d,z=z-h,h=h,h_d=h_d);
    screw_pole(x=x-d,y=x-d,z=h-w,h=h,h_d=h_d);
    screw_pole(x=d,y=x-d,z=h-w,h=h,h_d=h_d);
    }
module fan_hole(x=fan_size,d=fan_d,w=wall,t=tolerance){
    translate([x/2,x/2,-t])
        cylinder(d=fan_d,h=w,$fn=2*fn);
    }
module fan_duct(x=i_x0-spacing,y=i_y0-spacing,f=fan_size,d=fan_d,w=wall,z=duct_length,fs_d=fan_screw_head_d,adjust=adjust,t=tolerance){
    module duct(x=x,y=y,f=f,d=d,w=w,z=z,t=t){
        hull(){
            translate([0,0,0])
                fan_hole();
            translate([f/2-d/2,6+adjust/2,z-w+t])
                hole(x=d,y=y-adjust,z=w);
            }
        }
    /*module duct_wall(x=x,y=y,f=f,d=d,w=w,z=z,t=t){
        hull(){
            translate([0,0,0])
            fan_base();
            translate([f/2-x/2,w+fs_d,z-w])
                minkowski(){
                    #hole(x=x,y=y,z=w-t);
                    cylinder(r=w,h=t,$fn=fn);
                    }
            }
        }*/
    module duct_wall(x=x+w,y=y,f=f,d=d,w=w,z=duct_length,t=t){
        hull(){
            translate([0,0,0])
            fan_base();
            translate([f/2-x/2,0,0])
                fan_base(x=x,z=z);
            }

        }
    difference(){
        union(){
            duct_wall();
            //fan_screw_posts();
        }
        union(){
            //fan_screw_posts_remove();
            fan_screw_holes();
            duct();
            }
        }
    }

fan_duct();

*difference(){
    union(){
        base();
        base_walls();
        }
    union(){
        screws();
        holes();
        }
    }
