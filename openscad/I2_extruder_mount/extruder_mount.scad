/*  Copyright 2020 Dulio Samayoa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

rod_diameter=8;
rod_spacing=60;
wall_size=4;
screw_diameter=3;

$fn=60;

module rods(h=400,s=rod_spacing/2, r=rod_diameter/2) {
  translate([s, 0, 0])
    cylinder(r=r, h=h, center=true);
  translate([-s, 0, 0])
    cylinder(r=r, h=h, center=true);
}

module rod_clamp(r_d=rod_diameter, w_s=wall_size, reduction=0.9, s_d=screw_diameter) {
  h=1.62*r_d;
  d=r_d+2*w_s;

  difference() {
    union() {
      cylinder(d=d, h=h, center=true);
      translate([0, -d/4, 0])
	cube([reduction*d, d/2, h], center=true);
    }
    union() {
      cylinder(d=r_d,h=1+h, center=true);
      translate([0, -(1+h)/2, 0])
	cube([reduction*r_d, 1+h, 1+h], center=true);
      translate([0, -(r_d/2+s_d/2), 0])
      rotate([0, 90, 0])
	cylinder(d=s_d, h=d+1, center=true);
    }
  }
}

module extruder_mount(r_d=rod_diameter, r_s=rod_spacing, w=wall_size, s_d=screw_diameter) {
  part_width=1.62*r_d;

  //t_h=sqrt(pow(r_s,2)-pow(r_s/2,2));
  t_h=r_s*sin(60);
  t_r=r_s/2*tan(30);

  translate([t_r*cos(30), t_r*sin(30)+t_r+r_d/2+w/2, 0]) {
    rotate([0, 0, 30]) {
      difference() {
        cube([w, r_s,part_width], center=true);
	rotate([0,90,0]) cylinder(d=s_d,h=1+part_width, center=true);
      }
    }
  }

  translate([-t_r*cos(30), t_r*sin(30)+t_r+r_d/2+w/2, 0]) {
    rotate([0, 0, (30+120)]) {
      difference() {
        cube([w, r_s,part_width], center=true);
	rotate([0,90,0]) cylinder(d=s_d,h=1+part_width, center=true);
      }
    }
  }
  
  //translate([-t_r*cos(30), t_r*sin(30)+t_r+r_d/2+w/2, 0])
  //rotate([0,0,(30+120)])
  //cube([w, r_s,part_width], center=true);

  translate([0, r_d/2+w/2, 0])
  rotate([0,0,(30+240)])
  cube([w, r_s,part_width], center=true);

  translate([0, t_h+r_d/2+w/2, 0])
    cylinder(d=w, h=part_width, center=true);

}

//#rods();

module final() {
  translate([-rod_spacing/2, 0, 0])
    rod_clamp();
  translate([rod_spacing/2, 0, 0])
    rod_clamp();

  extruder_mount();
}

final();
